package model.entities;

import java.util.List;

public class Section {
	private String sec_id;
	private String semester;
	private int year;
	private String building;
	private String room_number;
	private String time_slot_id;
	private Classroom classroom;
	private Course course;
	private List<Instructor> instructor;
	private List<Student> student;
	}
