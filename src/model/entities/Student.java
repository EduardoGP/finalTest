package model.entities;

import java.util.List;

public class Student {
	private String student_id;
	private String name;
	private String dept_name;
	private double tot_cred;
	private List<Instructor> instructor;
	private List<Section> section;
}
