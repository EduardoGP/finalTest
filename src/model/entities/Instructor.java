package model.entities;

import java.util.List;

public class Instructor {
	private String id;
	private String name;
	private Department department;
	private double salary;
	private List<Section> section;
	private List<Student> student;
}
